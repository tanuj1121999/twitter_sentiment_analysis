# README #

This project is to analyse the sentiment of a tweet and classify it as positive or negative.The data
is taken from stanford corpus.Various classification algorithms are used such as logistic regression,
bernoulli naive bayes and multinomial naive bayes.Metrics used for evalution are precision,recall,f1score,accuracy,roc score.

Libraries used are numpy,pandas,nltk.
